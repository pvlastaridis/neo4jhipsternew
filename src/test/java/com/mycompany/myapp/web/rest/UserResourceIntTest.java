package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.Application;
import com.mycompany.myapp.PersistenceContext;
import com.mycompany.myapp.domain.Authority;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.AuthorityRepository;
import com.mycompany.myapp.repository.UserRepository;
import com.mycompany.myapp.security.AuthoritiesConstants;
import com.mycompany.myapp.service.UserService;
import com.mycompany.myapp.service.util.RandomUtil;
import com.mycompany.myapp.web.rest.dto.ManagedUserDTO;
import com.mycompany.myapp.web.rest.dto.UserDTO;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.inject.Inject;
import javax.transaction.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for the UserResource REST controller.
 *
 * @see UserResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PersistenceContext.class)
@WebAppConfiguration
@IntegrationTest
public class UserResourceIntTest {

    @Inject
    private UserRepository userRepository;

    @Inject
    private AuthorityRepository authorityRepository;

    @Inject
    private UserService userService;

    @Inject
    private PasswordEncoder passwordEncoder;

    private MockMvc restUserMockMvc;

    @Before
    public void setup() {
        UserResource userResource = new UserResource();
        ReflectionTestUtils.setField(userResource, "userRepository", userRepository);
        ReflectionTestUtils.setField(userResource, "userService", userService);
        this.restUserMockMvc = MockMvcBuilders.standaloneSetup(userResource).build();
    }

    @Test
    @Transactional
    public void testUpdateUserInfo() throws Exception {

        User newUser = new User();
        Authority authority = authorityRepository.findOneByName("ROLE_USER");
        Authority authority2 = null;
        if (authority==null) {
            Authority a = new Authority();
            a.setName("ROLE_USER");
            authority = authorityRepository.save(a);
            Authority a2 = new Authority();
            a2.setName("ROLE_ADMIN");
            authority2 = authorityRepository.save(a2);
        }
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode("1234");
        newUser.setLogin("pvlastaridis");
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName("Panos");
        newUser.setLastName("Vlastaridis");
        newUser.setEmail("panosvlastaridis@gmail.com");
        newUser.setLangKey("en");
        newUser.setCreatedDate(DateTime.now().getMillis());
        newUser.setResetDate(DateTime.now().getMillis());
        newUser.setActivated(true);
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        authorities.add(authority2);
        newUser.setAuthorities(authorities);
        User us = userRepository.save(newUser);

        ManagedUserDTO u = new ManagedUserDTO();
        u.setActivated(true);
        Set<String> authoritiess = new HashSet<>();
        authoritiess.add("ROLE_ADMIN");
        authoritiess.add("ROLE_USER");
        u.setAuthorities(authoritiess);
        u.setEmail("panosvlastaridis@gmail.com");
        u.setFirstName("Panossss");
        u.setLastName("Vlastaridis");
        u.setLangKey("en");
        u.setId(us.getId().toString());
        u.setLogin("pvlastari");
        u.setPassword("sth");
        restUserMockMvc.perform(
        put("/api/users")
        .contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(u)))
            .andExpect(status().isOk());
        User user = userRepository.findOneByLogin("pvlastari");
        assertThat(user!=null).isTrue();
    }

    @Test
    @Transactional
    public void testUpdateUserInfo2() throws Exception {

        User newUser = new User();
        Authority authority = authorityRepository.findOneByName("ROLE_USER");
        Authority authority2 = null;
        if (authority==null) {
            Authority a = new Authority();
            a.setName("ROLE_USER");
            authority = authorityRepository.save(a);
            Authority a2 = new Authority();
            a2.setName("ROLE_ADMIN");
            authority2 = authorityRepository.save(a2);
        }
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode("1234");
        newUser.setLogin("pvlastaridis2");
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName("Panos");
        newUser.setLastName("Vlastaridis");
        newUser.setEmail("panosvlastaridis2@gmail.com");
        newUser.setLangKey("en");
        newUser.setCreatedDate(DateTime.now().getMillis());
        newUser.setResetDate(DateTime.now().getMillis());
        newUser.setActivated(true);
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        authorities.add(authority2);
        newUser.setAuthorities(authorities);
        User us = userRepository.save(newUser);

        ManagedUserDTO u = new ManagedUserDTO();
        u.setActivated(true);
        Set<String> authoritiess = new HashSet<>();
        authoritiess.add("ROLE_ADMIN");
        authoritiess.add("ROLE_USER");
        u.setAuthorities(authoritiess);
        u.setEmail("panosvlastaridis@gmail.com");
        u.setFirstName("Panossss");
        u.setLastName("Vlastaridis");
        u.setLangKey("en");
        u.setId(us.getId().toString());
        u.setLogin("pvlastar");
        u.setPassword("sth");
        restUserMockMvc.perform(
            put("/api/users")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(u)))
            .andExpect(status().isOk());
        User user = userRepository.findOneByLogin("pvlastaridis2");
        assertThat(user!=null).isTrue();
    }

//    @Test
//    public void testGetExistingUser() throws Exception {
//        restUserMockMvc.perform(get("/api/users/pvlastaridis")
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType("application/json"))
//                .andExpect(jsonPath("$.lastName").value("Vlastaridis"));
//    }
//
//    @Test
//    public void testGetUnknownUser() throws Exception {
//        restUserMockMvc.perform(get("/api/users/unknown")
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isNotFound());
//    }
}
