'use strict';

angular.module('neo4jhipsterApp')
    .factory('errorHandlerInterceptor', function ($q, $rootScope) {
        return {
            'responseError': function (response) {
                if (!(response.status == 401 && response.data.path.indexOf("/api/account") == 0 )){
	                $rootScope.$emit('neo4jhipsterApp.httpError', response);
	            }
                return $q.reject(response);
            }
        };
    });