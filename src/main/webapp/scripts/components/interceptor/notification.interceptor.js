 'use strict';

angular.module('neo4jhipsterApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-neo4jhipsterApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-neo4jhipsterApp-params')});
                }
                return response;
            }
        };
    });
