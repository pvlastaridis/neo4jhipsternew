/* globals $ */
'use strict';

angular.module('neo4jhipsterApp')
    .directive('neo4jhipsterAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
