/* globals $ */
'use strict';

angular.module('neo4jhipsterApp')
    .directive('neo4jhipsterAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
