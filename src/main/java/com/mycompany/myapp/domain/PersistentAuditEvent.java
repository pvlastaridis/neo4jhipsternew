package com.mycompany.myapp.domain;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Set;

/**
 * Persist AuditEvent managed by the Spring Boot actuator
 * @see org.springframework.boot.actuate.audit.AuditEvent
 */
@NodeEntity
public class PersistentAuditEvent implements Serializable {

    @GraphId
    Long grid;

    private String id;

    @NotNull
    private String principal;

    private Long auditEventDate;

    private String auditEventType;

    @Relationship
    private Set<PersistentAuditEventData> data = new HashSet<>();

    public Long getGrid() {
        return grid;
    }

    public void setGrid(Long grid) {
        this.grid = grid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public Long getAuditEventDate() { return auditEventDate; }

    public void setAuditEventDate(Long auditEventDate) { this.auditEventDate = auditEventDate; }

    public LocalDateTime getAuditEventDDate() {
        Instant instant = Instant.ofEpochMilli(auditEventDate);
        LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneOffset.systemDefault());
        return ldt; }

    public void setAuditEventDDate(LocalDateTime auditEventDate) { this.auditEventDate =
        auditEventDate.toInstant(ZoneOffset.ofTotalSeconds(7200)).toEpochMilli(); }

    public String getAuditEventType() {
        return auditEventType;
    }

    public void setAuditEventType(String auditEventType) {
        this.auditEventType = auditEventType;
    }

    public Set<PersistentAuditEventData> getData() {
        return data;
    }

    public void setData(Set<PersistentAuditEventData> data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || grid == null || getClass() != o.getClass()) return false;

        PersistentAuditEvent entity = (PersistentAuditEvent) o;

        if (!grid.equals(entity.grid)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (grid == null) ? -1 : grid.hashCode();
    }
}
