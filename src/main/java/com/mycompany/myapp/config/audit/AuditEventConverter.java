package com.mycompany.myapp.config.audit;

import com.mycompany.myapp.domain.PersistentAuditEvent;
import com.mycompany.myapp.domain.PersistentAuditEventData;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class AuditEventConverter {

    /**
     * Convert a list of PersistentAuditEvent to a list of AuditEvent
     *
     * @param persistentAuditEvents the list to convert
     * @return the converted list.
     */
    public List<AuditEvent> convertToAuditEvent(List<PersistentAuditEvent> persistentAuditEvents) {

        if (persistentAuditEvents == null) return Collections.emptyList();
        List<AuditEvent> auditEvents = persistentAuditEvents.stream().map(this::convertToAuditEvent).collect(Collectors.toList());
        return auditEvents;
    }

    /**
     * Convert a PersistentAuditEvent to an AuditEvent
     *
     * @param persistentAuditEvent the event to convert
     * @return the converted list.
     */
    public AuditEvent convertToAuditEvent(PersistentAuditEvent persistentAuditEvent) {
        Instant instant = persistentAuditEvent.getAuditEventDDate().atZone(ZoneId.systemDefault()).toInstant();
        return new AuditEvent(Date.from(instant), persistentAuditEvent.getPrincipal(),
            persistentAuditEvent.getAuditEventType(), convertDataToObjects(persistentAuditEvent.getData()));
    }

    /**
     * Internal conversion. This is needed to support the current SpringBoot actuator AuditEventRepository interface
     *
     * @param data the data to convert
     * @return a map of String, Object
     */
    public Map<String, Object> convertDataToObjects(Set<PersistentAuditEventData> data) {
        Map<String, Object> results = new HashMap<>();

        if (data != null) {
            for (PersistentAuditEventData obj : data) {
                results.put(obj.getName(), obj);
            }
        }

        return results;
    }
}
