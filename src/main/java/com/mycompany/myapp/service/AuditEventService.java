package com.mycompany.myapp.service;

import com.mycompany.myapp.config.audit.AuditEventConverter;
import com.mycompany.myapp.domain.PersistentAuditEvent;
import com.mycompany.myapp.repository.PersistentAuditEventRepository;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service for managing audit events.
 * <p/>
 * <p>
 * This is the default implementation to support SpringBoot Actuator AuditEventRepository
 * </p>
 */
@Service
public class AuditEventService {

    private PersistentAuditEventRepository persistenceAuditEventRepository;

    private AuditEventConverter auditEventConverter;

    private final Logger log = LoggerFactory.getLogger(AuditEventService.class);


    @Inject
    public AuditEventService(
        PersistentAuditEventRepository persistenceAuditEventRepository,
        AuditEventConverter auditEventConverter) {

        this.persistenceAuditEventRepository = persistenceAuditEventRepository;
        this.auditEventConverter = auditEventConverter;
    }

    public List<AuditEvent> findAll() {
        List<PersistentAuditEvent> list = new ArrayList<>();
        for (PersistentAuditEvent p :persistenceAuditEventRepository.findAll()) {
            list.add(p);
        }
        return auditEventConverter.convertToAuditEvent(list);
    }

    public List<AuditEvent> findByDates(LocalDateTime fromDate, LocalDateTime toDate) {

        List<PersistentAuditEvent> persistentAuditEvents;
        ZoneId zoneId = ZoneId.systemDefault();

        persistentAuditEvents = persistenceAuditEventRepository.findAllByAuditEventDateBetween(
            fromDate.atZone(zoneId).toInstant().toEpochMilli(),
            toDate.atZone(zoneId).toInstant().toEpochMilli());
        return auditEventConverter.convertToAuditEvent(persistentAuditEvents);
    }

    public Optional<AuditEvent> find(String id) {
        return Optional.ofNullable(persistenceAuditEventRepository.findOneById(id)).map
            (auditEventConverter::convertToAuditEvent);
    }
}
