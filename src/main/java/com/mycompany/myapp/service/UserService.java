package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Authority;
import com.mycompany.myapp.domain.PersistentToken;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.AuthorityRepository;
import com.mycompany.myapp.repository.PersistentTokenRepository;
import com.mycompany.myapp.repository.UserRepository;
import com.mycompany.myapp.security.SecurityUtils;
import com.mycompany.myapp.service.util.RandomUtil;
import java.time.ZonedDateTime;
import java.time.LocalDate;

import com.mycompany.myapp.web.rest.dto.ManagedUserDTO;
import com.mycompany.myapp.web.rest.dto.PersistentTokenDTO;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.*;

/**
 * Service class for managing users.
 */
@Service
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    @Inject
    private PasswordEncoder passwordEncoder;

    @Inject
    private UserRepository userRepository;

    @Inject
    private PersistentTokenRepository persistentTokenRepository;

    @Inject
    private AuthorityRepository authorityRepository;

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                userRepository.save(user);
                log.debug("Activated user: {}", user);
                return user;
            });
        return Optional.empty();
    }

    public User completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);
        User user = userRepository.findOneByResetKey(key);
        if (user != null) {
            ZonedDateTime oneDayAgo = ZonedDateTime.now().minusHours(24);
            if (user.getResetDDate().isAfter(oneDayAgo)) {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                userRepository.save(user);
                return user;
            } else {
                return null;
            }
        }
        return user;
    }

    public User requestPasswordReset(String mail) {
        User user = userRepository.findOneByEmail(mail);
        if (user != null) {
            if (user.getActivated()) {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDDate(ZonedDateTime.now());
                userRepository.save(user);
                return user;
            } else {
                return null;
            }
        }
        return user;
    }

    public User createUserInformation(String login, String password, String firstName, String lastName, String email,
                                      String langKey) {

        User newUser = new User();
        Authority authority = authorityRepository.findOneByName("ROLE_USER");
        Authority authority2 = null;
        if (authority == null) {
            Authority a = new Authority();
            a.setName("ROLE_USER");
            authority = authorityRepository.save(a);
            Authority a2 = new Authority();
            a2.setName("ROLE_ADMIN");
            authority2 = authorityRepository.save(a2);
        }
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(login);
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setLangKey(langKey);
        newUser.setCreatedDate(DateTime.now().getMillis());
        newUser.setResetDate(DateTime.now().getMillis());
        // new user is not active
        newUser.setActivated(true);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        authorities.add(authority2);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public void updateUserInformation(String firstName, String lastName, String email, String langKey) {
        User u = userRepository.findOneByLogin(SecurityUtils.getCurrentUser().getUsername());
        if (u != null) {
            u.setFirstName(firstName);
            u.setLastName(lastName);
            u.setEmail(email);
            u.setLangKey(langKey);
            userRepository.save(u);
            log.info("Changed Information for User: {}", u);
        }
    }

    public void changePassword(String password) {
        User u = userRepository.findOneByLogin(SecurityUtils.getCurrentUser().getUsername());
        if (u != null) {
            String encryptedPassword = passwordEncoder.encode(password);
            u.setPassword(encryptedPassword);
            userRepository.save(u);
            log.debug("Changed password for User: {}", u);
        }
    }

    public User getUserWithAuthoritiesByLogin(String login) {
        User u = userRepository.findOneByLogin(login);
        if (u != null) {
            u.getAuthorities().size();
            return u;
        }
        return u;
    }

    public User getUserWithAuthorities(String id) {
        User user = userRepository.findOneById(id);
        user.getAuthorities().size(); // eagerly load the association
        return user;
    }

    public User getUserWithAuthorities() {
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUser().getUsername());
        user.getAuthorities().size(); // eagerly load the association
        return user;
    }

    /**
     * Persistent Token are used for providing automatic authentication, they should be automatically deleted after
     * 30 days.
     * <p/>
     * <p>
     * This is scheduled to get fired everyday, at midnight.
     * </p>
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void removeOldPersistentTokens() {
        LocalDate now = LocalDate.now();
        List<PersistentToken> list = new ArrayList<>();
        for (PersistentToken p : persistentTokenRepository.findByTokenDateBefore(now.minusMonths(1).toEpochDay())) {
            list.add(p);
        }
        list.stream().forEach(token -> {
            log.debug("Deleting token {}", token.getSeries());
            persistentTokenRepository.delete(token);
        });
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p/>
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     * </p>
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        ZonedDateTime now = ZonedDateTime.now();
        List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(now.minusDays(3));
        for (User user : users) {
            log.debug("Deleting not activated user {}", user.getLogin());
            userRepository.delete(user);
        }
    }

    @Transactional
    public User updateUserInformationMgt(ManagedUserDTO managedUserDTO) {
        Set<Authority> authorities = new HashSet<>();
        managedUserDTO.getAuthorities().stream().forEach(
            authority -> authorities.add(authorityRepository.findOneByName(authority))
        );
        User user = userRepository
            .findOne(Long.parseLong(managedUserDTO.getId()));
        if (user != null) {
            user.setLogin(managedUserDTO.getLogin());
            user.setFirstName(managedUserDTO.getFirstName());
            user.setLastName(managedUserDTO.getLastName());
            user.setEmail(managedUserDTO.getEmail());
            user.setActivated(managedUserDTO.isActivated());
            user.setLangKey(managedUserDTO.getLangKey());
            user.setAuthorities(authorities);
            User us = userRepository.save(user);
            return us;
        } else {
            return null;
        }

    }




    public User updateUserInformationMgt2(ManagedUserDTO managedUserDTO) {
               User user = userRepository
            .findOne(Long.parseLong(managedUserDTO.getId()));
        if (user!=null) {
            user.setLogin(managedUserDTO.getLogin());
            user.setFirstName(managedUserDTO.getFirstName());
            user.setLastName(managedUserDTO.getLastName());
            user.setEmail(managedUserDTO.getEmail());
            user.setActivated(managedUserDTO.isActivated());
            user.setLangKey(managedUserDTO.getLangKey());
            Set<Authority> authorities = user.getAuthorities();
            authorities.clear();
            managedUserDTO.getAuthorities().stream().forEach(
                authority -> authorities.add(authorityRepository.findOneByName(authority))
            );
            user.setAuthorities(authorities);
            User us = userRepository.save(user);
            log.info("Saved user firstname: {}", us.getFirstName());
            return us;
        } else {
            return null;
        }
    }

    public List<PersistentTokenDTO> getSessionByUser(String username) {
        User user = userRepository.findOneByLogin(username);
        List<PersistentTokenDTO> list = new ArrayList<>();
        if (user != null) {
            Iterable<PersistentToken> itr = persistentTokenRepository.findByUserCypher(user.getLogin());
            for (PersistentToken p : itr) {//user.getLogin())) {
                list.add(new PersistentTokenDTO((p)));
            }
        }
        return list;
    }
}
