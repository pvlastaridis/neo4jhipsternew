package com.mycompany.myapp.web.rest.dto;

import com.mycompany.myapp.domain.PersistentToken;

import javax.validation.constraints.Size;
import java.time.format.DateTimeFormatter;

/**
 * Created by Panos on 22-Nov-15.
 */
public class PersistentTokenDTO {


    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("d MMMM yyyy");

    private static final int MAX_USER_AGENT_LEN = 255;

    public PersistentTokenDTO(PersistentToken ps) {
        this.series = ps.getSeries();
        this.ipAddress = ps.getIpAddress();
        this.userAgent = ps.getUserAgent();
        this.formattedTokenDate = DATE_TIME_FORMATTER.format(ps.getTokenDDate());
    }

    private String series;

    //an IPV6 address max length is 39 characters
    @Size(min = 0, max = 39)
    private String ipAddress;

    private String userAgent;

    String formattedTokenDate;

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getFormattedTokenDate() {
        return this.formattedTokenDate;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        if (userAgent.length() >= MAX_USER_AGENT_LEN) {
            this.userAgent = userAgent.substring(0, MAX_USER_AGENT_LEN - 1);
        } else {
            this.userAgent = userAgent;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PersistentTokenDTO that = (PersistentTokenDTO) o;

        if (!series.equals(that.series)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return series.hashCode();
    }

    @Override
    public String toString() {
        return "PersistentToken{" +
            "series='" + series + '\'' +
            ", ipAddress='" + ipAddress + '\'' +
            ", userAgent='" + userAgent + '\'' +
            "}";
    }
}
