package com.mycompany.myapp.web.rest.dto;

import com.mycompany.myapp.domain.Authority;
import com.mycompany.myapp.domain.User;

import java.util.HashSet;
import java.util.Set;

/**
 * A DTO extending the UserDTO, which is meant to be used in the user management UI.
 */
public class ManagedUserDTO {

    private boolean activated;
    Set<String> authorities;
    String email;
    String firstName;
    String id;
    String langKey;
    String login;
    String lastName;
    String password;

    public ManagedUserDTO() {
    }

    public ManagedUserDTO(User user) {
        this.activated = user.getActivated();
        Set<String> set = new HashSet<>();
        for (Authority a : user.getAuthorities()) {
            set.add(a.getName());
        }
        this.authorities = set;
        this.email = user.getEmail();
        this.firstName = user.getFirstName();
        this.id = user.getId().toString();
        this.langKey = user.getLangKey();
        this.login = user.getLogin();
        this.lastName = user.getLastName();
        this.password = user.getPassword();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<String> authorities) {
        this.authorities = authorities;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return "ManagedUserDTO{" +
            "activated=" + activated +
            ", authorities=" + authorities +
            ", email='" + email + '\'' +
            ", firstName='" + firstName + '\'' +
            ", id='" + id + '\'' +
            ", langKey='" + langKey + '\'' +
            ", login='" + login + '\'' +
            ", lastName='" + lastName + '\'' +
            ", password='" + password + '\'' +
            '}';
    }
}
