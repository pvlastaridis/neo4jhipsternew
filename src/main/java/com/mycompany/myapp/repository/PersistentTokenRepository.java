package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.PersistentToken;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data Neo4j repository for the PersistentToken entity.
 */
@Repository
public interface PersistentTokenRepository extends GraphRepository<PersistentToken> {

    @Query("MATCH (n:PersistentToken)-[]->(:User {login:{0}}) RETURN n")
    Iterable<PersistentToken> findByUserCypher(String login);

    Iterable<PersistentToken> findByTokenDateBefore(Long localDate);

    PersistentToken findOneBySeries(String series);

}
